import createTheme from "@mui/material/styles/createTheme";

export default createTheme({
  palette: {
    primary: {
      main: "#FF4C29",
    },
    secondary: {
      main: "#334756",
    },
  },
});
