import React from "react";
import ThemeProvider from "@mui/material/styles/ThemeProvider";
import CssBaseline from "@mui/material/CssBaseline";
import GlobalStyles from "@mui/material/GlobalStyles";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import theme from "./libs/theme";
import routes from "./pages/routes/index";
import AuthProvider from "./pages/auth/provider";
import "./App.css"

function App() {
  return (
    <AuthProvider>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <CssBaseline />
        <BrowserRouter>
          <Switch>
            {routes.map((route, key) => (
              <Route
                key={key}
                exact={route.exact}
                path={route.path}
                render={(props) => (
                  <route.component routes={route.routes} {...props} />
                )}
              />
            ))}
          </Switch>
        </BrowserRouter>
      </ThemeProvider>
    </AuthProvider>
  );
}
export default App;
