import jwtDecode from "jwt-decode";

export const accessToken = () => {
  const token = localStorage.getItem("x-access-token");
  if (!token) {
    return null;
  }

  const { iat, exp, roles } = jwtDecode(token);
  const today = Number((Date.now() / 1000).toFixed(0));
  const iatS = today - iat;

  let rol = "";
  roles.map(({ name }) => {
    return (rol = name);
  });

  return iat + iatS > exp
    ? logout()
    : {
        token,
        roles: rol,
      };
};

export const logout = () => {
  localStorage.removeItem("x-access-token");
};
