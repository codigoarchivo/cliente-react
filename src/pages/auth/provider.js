import React from "react";
import { accessToken, logout } from "./token";
import { AuthContext } from "./context";

function CreateAccess({ children }) {
  const [user, setUser] = React.useState({
    status: false,
    loanding: false,
  });

  React.useMemo(() => {
    if (!accessToken()) {
      return logout();
    }
    setUser({
      status: !accessToken() ? false : true,
      loanding: !accessToken() ? false : accessToken(),
    });
  }, []);

  return <AuthContext.Provider value={user}>{children}</AuthContext.Provider>;
}
export default CreateAccess;
