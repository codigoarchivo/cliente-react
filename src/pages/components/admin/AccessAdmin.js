import React from "react";
import { AuthContext } from "../../auth/context";
import { Redirect } from "react-router-dom";

const Users = () => React.useContext(AuthContext);

const AccessAdmin = () => {
  const { status, loanding } = Users();
  return status === true && loanding.roles === "Admin" ? (
    <span>accessAdmin</span>
  ) : (
    <>
      <Redirect to="/access/404" />
    </>
  );
};
export default AccessAdmin;
