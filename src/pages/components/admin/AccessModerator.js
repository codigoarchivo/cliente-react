import React from "react";
import { AuthContext } from "../../auth/context";
import { Redirect } from "react-router-dom";

const Users = () => React.useContext(AuthContext);

const AccessModerator = () => {
  const { status, loanding } = Users();
  return (status === true && loanding.roles === "Moderator") ||
    loanding.roles === "Admin" ? (
    <>
      <span>AccessModerator</span>
    </>
  ) : (
    <>
      <Redirect to="/access/404" />
    </>
  );
};
export default AccessModerator;
