import React from "react";
import { Route, Switch } from "react-router-dom";
import Container from "@mui/material/Container";

function Global({ routes }) {
  return (
    <Container>
      <Switch>
        {routes.map((route, key) => (
          <Route
            key={key}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        ))}
      </Switch>
    </Container>
  );
}
export default Global;
