import React from "react";
import { Route, Switch } from "react-router-dom";

function Register({ routes }) {
  return (
    <>
      <Switch>
        {routes.map((route, key) => (
          <Route
            key={key}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        ))}
      </Switch>
    </>
  );
}
export default Register;
