import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Box from "@mui/material/Box";
import styled from "@mui/material/styles/styled";
import Typography from "@mui/material/Typography";
import Drawer from "./layouts/Drawer";
import { AuthContext } from "../auth/context";

const Users = () => React.useContext(AuthContext);

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

function Access({ routes }) {
  const { status } = Users();
  if (status === false) {
    return (
      <>
        <Redirect to="/" />
      </>
    );
  }

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Drawer />
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
          <DrawerHeader />
          <Typography paragraph>
            <Switch>
              {routes.map((route, key) => (
                <Route
                  key={key}
                  exact={route.exact}
                  path={route.path}
                  component={route.component}
                />
              ))}
            </Switch>
          </Typography>
        </Box>
      </Box>
    </>
  );
}
export default Access;
