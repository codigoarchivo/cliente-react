import Global from "../views/Global";

import Home from "../components/global/Home";
import About from "../components/global/About";
import Contact from "../components/global/Contact";

import Error404 from "../components/err/Error404";

const global = [
  {
    path: "/",
    component: Global,
    exact: false,
    routes: [
      {
        path: "/",
        component: Home,
        exact: true,
      },
      {
        path: "/about",
        component: About,
        exact: true,
      },
      {
        path: "/contact",
        component: Contact,
        exact: true,
      },
      {
        component: Error404,
      },
    ],
  },
];

export default global;
