import inUp from "./inUp.routes";
import access from "./access.routes";
import global from "./global.routes";

const routes = inUp.concat(access, global);

export default routes;
