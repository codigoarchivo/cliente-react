import Register from "../views/Register";

import SingUp from "../components/log/SingUp";
import SingUpAdmin from "../components/log/SingUpAdmin";
import SingIn from "../components/log/SingIn";

import Error404 from "../components/err/Error404";

const routes = [
  {
    path: "/entry",
    component: Register,
    exact: false,
    routes: [
      {
        path: "/entry",
        component: SingIn,
        exact: true,
      },
      {
        path: "/entry/users",
        component: SingUp,
        exact: true,
      },
      {
        path: "/entry/admin",
        component: SingUpAdmin,
        exact: true,
      },
      {
        component: Error404,
      },
    ],
  },
];
export default routes;
