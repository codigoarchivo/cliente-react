import Access from "../views/Access";

import AccessAdmin from "../components/admin/AccessAdmin";
import AccessModerator from "../components/admin/AccessModerator";
import AccessUsers from "../components/admin/AccessUsers";

import Error404 from "../components/err/Error404";

const Accesskey = [
  {
    path: "/access",
    component: Access,
    exact: false,
    routes: [
      {
        path: "/access",
        component: AccessAdmin,
        exact: true,
      },
      {
        path: "/access/moderator",
        component: AccessModerator,
        exact: true,
      },
      {
        path: "/access/users",
        component: AccessUsers,
        exact: true,
      },
      {
        component: Error404,
      },
    ],
  },
];
export default Accesskey;
